#!/usr/bin/julia -O0

using CRC32c, Dates

input_file = @__FILE__

if length(ARGS) < 1 || ARGS[1] ∈ ["--dry-run","-d"] # -d is valid, but still...
	output_dir = "--dump"
else
	output_dir = ARGS[1]
end

if occursin(r"--dump",output_dir)
	@info "Recieving as a dry-run. No subvolumes will be created."
else
	if ! isdir(output_dir)
		@warn "Recieving directory does not exist." output_file = output_file
		println("usage: $(@__FILE__) [output_dir/-d/--dry-run/--dump]")
		exit(1)
	end

	part_type = chomp(read(`stat -f --format="%T" $output_dir`,String) )
	if part_type != "btrfs"
		@warn "Destination directory, $output_dir is not on a btrfs volume." begin
			output_dir = local output_dir
		 	part_type = local part_type
		end
		exit(1)
	end
end

regex_crc32c = r"(.{8})\.zst\.gpg"
if occursin(regex_crc32c,input_file)
	embedded_crc = match(regex_crc32c,input_file).captures[1]
else
	@warn "No embedded CRC32c found."
	println("Either way, there's plenty of error checking baked into this archive.")
end

file_crc32c = bytes2hex(reverse(reinterpret(UInt8,[open(crc32c,input_file)])))
file_crc32c != embedded_crc ? (@warn "CRC32c error, continuing anyways") : nothing

struct Snapped

	# I really think I need to store these internally as strings...
	src_date::Date
	dst_date::Date
	data_crc32c::UInt32
	data::Array{UInt8}

	function Snapped(src_date::Date,dst_date::Date,data_crc32c::UInt32,data::Array{UInt8})
		data_crc32c != crc32c(data) ? error("CRC32c miss match") : new(
			src_date,dst_date,data_crc32c,data)
	end

end

# We should sort these with a sense of "closeness" in mind.
# This may require a custom "sort" instead...
# For now, let's just go with size
import Base: isless
isless(a::Snapped,b::Snapped) = isless(length(a.data),length(b.data))

function mk_snapped_array(decrypted::IO)

	N = read(decrypted,UInt8)

	header_checksum = reinterpret(UInt32,read(decrypted,4))[1]
	sizes_UInt8 = read(decrypted,8*N)
	dates_UInt8 = read(decrypted,4*N)
	io_checksums_UInt8 = read(decrypted,4*N)
	computed_header_checksum = crc32c([sizes_UInt8;dates_UInt8;io_checksums_UInt8])
	if computed_header_checksum != header_checksum
		@error "Checksum error"
			println("Expect: ",header_checksum)
			println("Got: ",computed_header_checksum)
		exit(1)
	end

	sizes = reinterpret(Int64,sizes_UInt8)
	dates_Int16 = reinterpret(Int16,reshape(dates_UInt8,(2*N,2)))
	dates = @. Day(dates_Int16) + Date("2021-01-01")
	io_checksums = reinterpret(UInt32,io_checksums_UInt8)

	decompress = `zstdcat --long=30`
	# This may result in copying data, but fuck it.
	decompressed = read(pipeline(decrypted,decompress))

	ios = Array{Array{UInt8},1}(undef,N)
	ios[1] = @view decompressed[1:sum(sizes[1])]
	for i = 2:N
		ios[i] = @view decompressed[1+sum(sizes[1:i-1]):sum(sizes[1:i])]
	end

	snapped_array = Array{Snapped,1}(undef,N)

	for i in 1:N
		snapped_array[i] = Snapped(dates[i,1],dates[i,2],io_checksums[i],ios[i])
	end

	# Do we actually need this now?
	if output_dir != "--dump"
		filter!(x -> ! (x.dst_date ∈ readdir(output_dir)),snapped_array)
	end

	return sort!(snapped_array)

end

# This fucntion is not performant.
# It computes a lot of stuff unnecessarily.
# In particular, we're creating fairly large arrays constantly.
# It's also not optimal. That would be the Edmond's algorithm for a MDST.
# 	https://www.cs.tau.ac.il/~zwick/grad-algo-13/directed-mst.pdf by : Uri Zwick,
# published April 22, 2013, has a lovely plan English implimentation of the algorithm
# in subsection 5.1. But also at the end of section 4:
# 	 Choose cheapest entering edges of vertices other than the root until either
# a DST is formed, in which case it is a MDST, or until a cycle C is formed. If
# a cycle C is formed, contract it and adjust the edge costs appropriately. Find
# an MDST in the contracted graph and expand it into a MDST of the original graph.
# 	It should be noted that it is possible for something to go wrong and the graph
# to not be strongly connected... So... that's not great. The lecture notes assumes
# strongly connected.
#
# For my purposes it's enough to pick an arbitrary node (the one with the smallest
# edge?) and then run the algorithem until I hit a node already written to disc.
# Then start writing nodes to disc as you collapse the super-nodes. Rinse and repeat
# until all nodes are reconstructed. In a single root case, this should result in
# the same tree as the one in those notes. In a multi-rooted case it should result
# in a minimal forest.
#
# This does have the down side of assuming all nodes need to be reconstructed. If
# instead I want to only reconstruct one node, the DESIRED tree is not minimal.
# An easy example is a long chain of cheap edges being prefered over an existing
# node right next to the one you want.
function useful_snappeds(snappeds::Array{Snapped})

	function is_useful(snapped::Snapped,exists::Array)

		if snapped.dst_date ∈ exists
			return false
		elseif snapped.src_date == snapped.dst_date
			return true
		end
		return snapped.src_date ∈ exists

	end

	availible = Date.(replace.(readdir(output_dir),r"\.snap" => s""))

	return filter(s -> is_useful(s,availible),snappeds) # Can return an empty set

end

function bytes2snappeds(password::String)

	temp_file = mktemp()[1]

	archive = open(input_file)
	while readline(archive) != "exit(0)"; end
	write(temp_file,archive)
	close(archive)

	decrypt = `gpg --batch --passphrase $password --decrypt $temp_file`

	return  read(decrypt) |> PipeBuffer |> mk_snapped_array

end

# Note tht this no longer counts errors. Perhaps have an errors = 0 in the args?
# Then pass them on from there?
function do_a_thing(
	dest::String,snapped_array::Array{Snapped},
	old_snappeds_list = Snapped[]
)

	snappeds_list = useful_snappeds(snapped_array)
	receive = Cmd(`sudo btrfs receive $dest`,ignorestatus=true)

	if isempty(snappeds_list)
		return nothing
	elseif snappeds_list == old_snappeds_list
		@error "Endless loop detected"
		return nothing
	else
		dst_date = snappeds_list[1].dst_date
		src_date = snappeds_list[1].src_date
		println("From: ",src_date," To: ",dst_date,"\n")
		run(
			pipeline(PipeBuffer(snappeds_list[1].data),receive,devnull)
		)
		return do_a_thing(dest,snapped_array,snappeds_list)
	end

end

function main(dest::String,snapped_array::Array{Snapped})

	receive = Cmd(`sudo btrfs receive $dest`,ignorestatus=true)

	errors = 0
	if output_dir == "--dump"
		for i in 1:length(snapped_array)
			dst_date = snapped_array[i].dst_date
			src_date = snapped_array[i].src_date
			println("From: ",src_date," To: ",dst_date,"\n")
			errors += run(
				pipeline(PipeBuffer(snapped_array[i].data),receive,devnull)
			).exitcode != 0
		end
	else
		do_a_thing(dest,snapped_array)
	end
	errors != 0 ? (@warn "Snapshots sent with $errors errors.") : println("No errors")

end

if ! @isdefined password
	print("\nEnter Password:")
	password = readline()
end

bytes2snappeds(password) |> s -> main(output_dir,s)

exit(0)
