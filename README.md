# Disclaimer
 
I'm not much of a programmer. This is just a little something I wrote to help ensure my data is protected against... life. This is one of a few ways I keep from losing data, one which I started December 2020 after noticing something wasn't working as expected with another method (and also maybe having lost a file... somehow).
 
This is meant to fill my own personal niche requirements and is unlikely to be suited for pretty much anyone else. For that matter, I imagine pretty much anyone else could have done a better job, using a more suitable language (Go?).
 
# Purpose
 
The purpose of this collection of scripts is to have a sort of "catch all" method of backing up several versions of my work in one go, and storing it all in one package. This is mostly to protect against human error. I have other, simpler scripts which produce smaller archives, but they're more discriminating. Either they only look for certain files and only store one full snapshot of the working state, or they rely on `git bundle` which has shown itself to be less than ideal.
 
This grabs the entire subvolume, so I can't miss anything unless I've marked something as `nocow` (which is something I've only done once and that was for a VM), or btrfs itself has done something stupid. I know btrfs doesn't have the greatest reputation when compared to ZFS, but it hasn't given me any headaches in the last four years, and in the nearly seven years I've used it for pretty much everything I've never lost data because of it. It also provides a way of collecting lightweight snapshots of the various states of my work. Better still, it has a way of packaging these snapshots (either whole or as a snapshot of another subvolume) and recreating the subvolume.
 
# Inner Workings
 
Therefore, this relies on three commands: `btrfs subvolume snapshot -r`, `btrfs send` and `btrfs receive`. It's really just a collection of Julia Script.
 
So I create a bunch of read-only snapshots `snap.jl`. With this collection I then use the included `send.jl` script to create a list of dates from which `btrfs send` is used to create a collection of data streams which can be `btrfs recieve`'d to recreate them. Each of these streams is created and stored seperately and indexed. Also included in each archive is a copy of `receive.jl` so they can be extracted.

At the time of extraction, the script searches for existing subvolumes and uses the offsets stored to judge an semi-optimal way of rebuilding all snapshots (I estimate the data effiency to be around 95–99% of what can be obtained with a proper MDST algorithm). In this way, a single archive can restore several subvolumes at once. Combined with compression provided by `zstd --long`, the actual amount of space added with each snapshot (itself already having undergone what can be considered delta compression) is quite small. For example, a typical subvolume is about 25MB. Compressed this is around 6.6MB. A "full-back up" consisting a complete copy of one snapshot and multiple differential backups of 42 others are 19MB. At least in my usage.
 
These data streams are combined into a single byte array and placed at the end of the archive after compression. The header is then made up of the number of snapshots being archived, dates for both the source and destination snapshot, checksums for the uncompressed data, and the sizes of each data stream once uncompressed. This is then encrypted. In front of all that is a script to unpack all this. Julia is still needed to unpack, so this isn't ideal, though I do have thoughts on how I could do this with a binary extractor if I ever transcode this. Of course, it is kind of like Python in that it's a simple matter to install. Also, the "how" of unpacking is effecively documented in the script so I could in theory do it by hand.
 
The scheme used to create these snapshots is currently to choose a "root" snapshot which is sent in full which is in the "center" of the date range. Then from that there is both incremental and decremental data to recreate other snapshots in the date range. **There is currently no way to know this date range without unpacking the archive!**
 
# Features
 
* Self-extracting and self-testing (So long as you have Julia installed.)
* Lots of (mostly unnecessary) checksums
* Reasonably space efficient. (At least in my opinion)
* Encrypted
* Code so easy to maintain, even I can do it! (Not a lot of it and Julia reads like pseudo-code.)
* Indexed streams so you don't have to send them all. (Important since all snapshots would fail if any one of them fails.)
* Per subvolume config files (of sorts).
* Pick the optimal (read: problably better than random) stream to restore only those snapshots which are missing.
* Differential backups in addition to the current ones.
 
# TODO
 
* Tweak sorting algorithm for better compression.
* A way to tell the dates or date range for each archive without unpacking.
* Better implimentation of existing algorithm for choosing the restoration order.
* Error Correction Code (Probably never going to happen.)
* Stand-alone extractor in case Julia can't be installed on the system.
* Unpacking utility to extract a specific date's snapshot from using a collection of archives.
* Create yearly super archive.
* Automated pruning system after creation of a "super archive".
