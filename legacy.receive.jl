#!/usr/bin/julia -O0

using CRC32c, Dates

if length(ARGS) != 2
	@warn "Not enough arguments. Got ",length(ARGS),", expected 2."
	println("usage: open-snap-bu.sh archive output_dir/-d/--dry-run/--dump")
	exit(1)
end

input_file,output_dir = ARGS


if output_dir == "--dry-run"
	output_dir = "--dump"
end


if ! isfile(input_file)
	@warn "Backup archive does not exist."
	println("usage: open-snap-bu.sh archive output_dir")
	exit(1)
end

if occursin(r"-d|--dump",output_dir)
	@info "Recieving as a dry-run. No subvolumes will be created."
else
	if ! isdir(output_dir)
		@warn "Recieving directory does not exist." output_file = output_file
		println("usage: open-snap-bu.sh archive output_dir")
		exit(1)
	end
	
	part_type = chomp(read(`stat -f --format="%T" $output_dir`,String) )
	if part_type != "btrfs"
		@warn "Destination directory, $output_dir is not on a btrfs volume." begin
			output_dir = local output_dir
		 	part_type = local part_type
		end
		exit(1)
	end
end

struct SnappedArray

	sizes::Array{Int,1}
	io_checksums::Array{UInt32,1}
	dates::Array{Date,2}
	# comperssed_checksums::Array{UInt32,1} # divide compressed up and checksum
	# It may be a good idea (at a later point) to make these with Arrays of UInt8.
	# In the case of ios, that would need to be Array{Array{UInt8}}. Actually,
	# that would be as easy as setting compressed = compressed_snaps(ios,size).data
	ios::Array{IO,1}

end

function SnappedArray(decrypted::IO)

	N = read(decrypted,UInt8) # Int64 */÷/+/- UInt8 = Int64
	# @info N

	header_checksum = reinterpret(UInt32,read(decrypted,4))[1]
	sizes_UInt8 = read(decrypted,8*N)
	dates_UInt8 = read(decrypted,4*N)
	io_checksums_UInt8 = read(decrypted,4*N)
	computed_header_checksum = crc32c([sizes_UInt8;dates_UInt8;io_checksums_UInt8])
	if computed_header_checksum != header_checksum
		@error "Checksum error"
			println("Expect: ",header_checksum)
			println("Got: ",computed_header_checksum)
		# exit(1)
	end

	sizes = Int64.(reinterpret(Int64,sizes_UInt8))
	dates_Int16 = reinterpret(Int16,reshape(dates_UInt8,(2*N,2)))
	dates = @. Day(dates_Int16) + Date("2021-01-01")
	io_checksums = reinterpret(UInt32,io_checksums_UInt8)
	
	# What's left of decrypt is the compressed ios.
	decompress = `zstdcat --long=30`
	# This may result in copying data, but fuck it.
	decompressed = read(pipeline(decrypted,decompress))


	# as much as I'd like to use view, it IOBuffer can't take SubArrays :(
	ios = Array{IOBuffer,1}(undef,N)
	ios[1] = IOBuffer(decompressed[1:sum(sizes[1])])
	for i = 2:N
		ios[i] = IOBuffer(decompressed[1+sum(sizes[1:i-1]):sum(sizes[1:i])])
	end

	return SnappedArray(sizes,io_checksums,dates,ios)

end

################################################################################

function do_a_thing(dest::String,password::String)
	
	temp_file = mktemp()[1]

	archive = open(input_file)
	while readline(archive) != "exit(0)"; end
	write(temp_file,archive)
	close(archive)

	decrypt = `gpg --batch --passphrase $password --decrypt $temp_file`
	receive = `sudo btrfs receive $dest`
	# receive = pipeline(`sudo btrfs receive $dest`,stdout=devnull)
	# Maybe one day I'll store a list of changed files instead of sending that info
	# off to /dev/null?

	snapped_array = PipeBuffer(read(decrypt)) |> SnappedArray

	errors = 0
	for (i,io) in enumerate(seekstart.(snapped_array.ios))
		println("From: ",snapped_array.dates[i,1],", To: ",snapped_array.dates[i,2])
		if run(pipeline(io,receive,devnull)).exitcode != 0
			@warn "snapshoting for some snapshot"
			errors += 1
		end
	end
	println("Snapshots sent with $errors errors.")

end

regex_crc32c = r"(.{8})\.zst\.gpg"
regex_crc32  = r"zst\.\[(.{8})\]\.gpg"

cont = false
if occursin(regex_crc32c,input_file)
	embedded_crc =  match(regex_crc32c,input_file).captures[1]
	cont = true
elseif occursin(regex_crc32,input_file)
	@warn "This appears to be an older file. Please verify with rhash."
else
	@warn "Something dun fucked up. Check the file and the script."
	exit(2)
end

if cont

	file_crc32c = bytes2hex(reverse(reinterpret(UInt8,[open(crc32c,input_file)])))
	
	if file_crc32c == embedded_crc

		if ! @isdefined password
			print("\nEnter Password:")
			password = readline()
		end

		do_a_thing(output_dir, password)

	else
		@warn "CRC32c error"
	end

elseif run(Cmd(`rhash -k $input_file`,ignorestatus=true)).exitcode == 0

	if ! @isdefined password
		print("\nEnter Password:")
		password = readline()
	end

	do_a_thing(input_file,password)

else
	@warn "rhash $input_file failed."
	println("I have no idea why since this script isn't exactly meant for that.")
	println("Or maybe it's something else. I don't know.")
	exit(2)
end
