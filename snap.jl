#!/usr/bin/julia -O0

using CRC32c, Dates

mnt = chomp(read(`stat --format=%m $(pwd())`,String))
# TODO: We need to make this file if it doesn't exist.

part_type = chomp(read(`stat -f --format="%T" $mnt`,String) )
if part_type != "btrfs"
	@error "Destination directory is not on a btrfs volume." mnt part_type
	exit(1)
end

config = mnt*"/.btrfsarchiverrc"
include(config)

if ! isdir(snaps_dir)
	mkpath(snaps_dir,mode=0o755)
end

if isdir(joinpath(snaps_dir,new_snap))

	println("The subvolume ",joinpath(snaps_dir,new_snap)," already exists.")
	println("If you wish to delete it type \"sub del\" or \"exit\" to exit.")

	local del = ""
	while ! contains(del, r"sub del|exit")
		del = readline()
		if del == "sub del"
			run(`sudo btrfs sub del $snaps_dir/$new_snap`)
		elseif del == "exit"
			exit(0)
		end
	end

end

old_snap = chomp(read(pipeline(
	`ls $snaps_dir --color=no`,
	`grep -E "202.*snap"`,
	`grep -v zst`,`tail --lines=1`
),String))

run(`sudo btrfs sub snap -r $mnt $snaps_dir/$new_snap`)

# I don't think n does anything any more
function doiit(n=interval)

	dayvalue(x) = x.instant.periods.value

	rx_sent = Regex("$subvol\\.(202\\d-\\d\\d-\\d\\d)\\.snap\\.[a-f0-9]{8}\\.zst\\.gpg")
	lastsent = filter(x->occursin(rx_sent,x),readdir(archive_dir))[end]
	lastsent_date = length(subvol) |> x -> Date(SubString(lastsent,x+2,x+11))

	rx_snap = r"20\d{2}-[01]\d-[0-3]\d\.snap$"
	snaps = sort(filter(x->occursin(rx_snap,x),readdir(snaps_dir)))
	snaps_date = Date.(SubString.(snaps,1,10))
	days_since_lastsent = dayvalue(today()) - dayvalue(lastsent_date)
	snap_since_lastsent = length(filter(x-> x > lastsent_date , snaps_date))

	return snap_since_lastsent >= interval || days_since_lastsent == 0
	# return days_since_lastsent + snap_since_lastsent >= 2*interval || days_since_lastsent == 0

end

if doiit()
	println("\nArchiving Snapshots\n")
	include(@__DIR__()*"/send.jl")
end
