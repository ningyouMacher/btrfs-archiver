#!/usr/bin/julia -O0

using Dates, CRC32c

config = chomp(read(`stat --format=%m $(pwd())`,String))*"/.btrfsarchiverrc"

const test = false
if test
	@warn "Using test settings"
end
include(config)

getfirst(rx,str) = (first∘getfield)(match(rx,str),:captures)

# Ideally the interval should be odd. Evens require that for the differential part, the
# backwards goes ceil(1.5*inverval-1/2)) and floor(that) in the other... or the
# other way around... I think. Either way, it's easier in the case of odds due to
# symetry. You go to the next root which is the interval, and then half-way past
# that. For odd intervals, there is an even number of intermediate snap shots between
# roots.

checkgit() = run(Cmd(`git diff-index --quiet HEAD --`,ignorestatus=true)).exitcode != 0
if cd(checkgit,@__DIR__())
	@warn "Snapshot repo not clean."
	println("Hit enter to continue, and anything else to exit")
	if length(readline()) != 0
		exit(0)
	end
	repoclean = false
else
	repoclean = true
end

# We want to reach for 6*interval for a reason.
# 	With the scheme I have in mind, this will result in a full root every 3 backups
# each of which will point to the root in the previous and next with a differential
# backup which seems less prone to errors since the chain is shorter.
# 	That explains the 2*interval, but where does the extra factor of 3 come from?
# Answer: Because I want to cover those full roots with incremental/decremental
# backups that can be had from a single full root'ed back up archive. So really,
# it's not an extra factor of 3 so much as another 4 jumps (two in both directions).
function list_snaps(N=6*interval+1) #+1 for the root

	snaps = sort(filter(
		x->occursin(r"20\d{2}-[01]\d-[0-3]\d\.snap$",x),readdir()
	))

	if lastindex(snaps) < N
		return snaps
	else
		return snaps[end-N+1:end]
	end
end

struct Snapped

	# I may want to change the Dates to Int64 since this is what will be read
	# and written and converting to and from is a pain... but maybe a string is
	# even better?
	# Maybe a pair like, src_date => dst_date for nice syntax?
	src_date::Date
	dst_date::Date
	data_crc32c::UInt32
	data::Array{UInt8}

	function Snapped(src_date::Date,dst_date::Date,data::Array{UInt8})
		data_crc32c = crc32c(data)
		new(src_date,dst_date,data_crc32c,data)
	end

end

function Snapped(src_str::String,dst_str::String)

	src_date = Date(replace(src_str,r"\.snap" => s""))
	dst_date = Date(replace(dst_str,r"\.snap" => s""))

	if src_date == dst_date
		io = read(`sudo btrfs send $dst_str`)
	else
		io = read(`sudo btrfs send -p $src_str $dst_str`)
	end

	return Snapped(src_date,dst_date,io)

end

# https://discourse.julialang.org/t/sorting-arrays-of-structures/18725/3
import Base: isless

# From a handful of experiments I found compression is better when roots are first
# then differencials. In fact, with differencials up front the size ends up less
# than 1% larger. More testing needed for more intriticate orders.
function isless(a::Snapped,b::Snapped)
	# Root first provides better compression
	# It may be better to just go by size once I have a sorting algorithm on the
	# recieving end.
	if a.dst_date == a.src_date
		return true
	elseif a.dst_date == b.dst_date
		return a.src_date <= b.src_date
	else
		return a.dst_date < b.dst_date
	end
end

function make_snappeds(snaps::Array{String})

	# Some of this logic could be simplied by tacking on "full" to the file name
	# of full back ups...
	function full_bu(n=3,m=3) # m's 3 is just an arbitrary choice, unlike n's.

		if n > 3
			@warn "n > 3, back up strength will be weakened."
		end

		names2dates(regex,Sarray) = (Date∘first∘getfield).(match.(regex,Sarray),:captures)

		rx = Regex("$subvol\\.(202\\d-\\d\\d-\\d\\d)\\.snap\\.[a-f0-9]{8}\\.zst\\.gpg")
		archives = sort(filter(x->occursin(rx,x),readdir(archive_dir)))
		lastsent = archives[end-n+1:end]
		lastsent_date = names2dates(rx,lastsent)

		rx_snap = r"(20\d{2}-[01]\d-[0-3]\d)\.*?snap$"
		lastsnap = sort(filter(x->occursin(rx_snap,x),readdir(snaps_dir)))[end-interval:end]
		lastsnap_date = names2dates(rx_snap,lastsnap)

		B = length(archives) % n == 0
		E = lastsnap_date[1] > lastsent_date[end]
		D = length(archives) < m

		return B || E || D

	end

	# n in full_bu() shouldn't be more than 3! This 6*interval stuff breaks. If
	# it is, then at least we should have no more than 3*interval snaps between
	# full bu's. Though there isn't any really good way to pick them out without
	# putting something into the file name... which I don't want to do... for some
	# reason.
	function toomany_snaps_since_lastfull(n=3)

		rx_snap = r"20\d{2}-[01]\d-[0-3]\d\.snap$"
		lastsnap = sort(filter(x->occursin(rx_snap,x),readdir(snaps_dir)))[end-n+1:end]
		lastsnap_date = Date.(SubString.(lastsnap,1,10))

		return false

	end

	N = length(snaps)

	if force_full || full_bu(3) || toomany_snaps_since_lastfull()
		global full = true # Why global? Just in case.
		println("Starting Full Back Up")
		sleep(1)
	else
		global full = false # Why global? Just in case.
		println("Starting Partial Back Up")
		sleep(1)
	end

	array_size = 2(N-1) + full + 3(interval-1)
	snapped_array = Array{Snapped,1}(undef,array_size)

	# It may be better to move the root index up closer to the front.
	# This should improve compression and in general, I'm keener on decremental
	# stuff.
	index_r = floor(Int,N/2)+1
	diff_offset = array_size - 3(interval-1)

	for i in 1:N-1
		snapped_array[i] =		Snapped(snaps[i],snaps[i+1])
		snapped_array[N+i-1] =	Snapped(snaps[i+1],snaps[i])
	end
	if full
		snapped_array[diff_offset] = Snapped(snaps[index_r],snaps[index_r])
	end

	if iseven(interval)
		@warn "Interval is even. Differential distribution won't be symmetric."
		# Something = something I'll work out later! The interval is 5 so who cares!
		# snapped_array[end] = Snapped(snaps[index_r],snaps[something])
	end
	for i = 1:floor(Int,3*(interval-1)/2)
		snapped_array[2i+diff_offset] = Snapped(snaps[index_r],snaps[index_r+1+i])
		snapped_array[2i-1+diff_offset] = Snapped(snaps[index_r],snaps[index_r-1-i])
	end

	return sort(snapped_array)

end

function snappeds2bytes(array::Array{Snapped})

	function compressor(size::Int; long_limit=25.5::AbstractFloat)

		long = min(log(2,size),30)

		if test
			return `zstd -1`
		elseif long > long_limit # Too small and it hurts compression...
			return `zstd --ultra -$compression_level --long=$(ceil(Int,long))`
		else
			return `zstd --ultra -$compression_level`
		end

	end

	function compress_data(datas::Array{Array{UInt8,1}},size::Int)

		compressed = PipeBuffer()

		uncompressed_size = open(pipeline(compressor(size),compressed),write=true) do compress
			write(compress,datas...)
		end

		if uncompressed_size != size
			@warn "Bytes written doesn't equal bytes expected."
		end

		return compressed.data

	end

	function mkheader(sizes::Array{Int64},io_checksums::Array{UInt32},dates::Array{Date})

		# The use of reinterpret here isn't ideal. It doesn't consistantly result
		# in the same output as, say recasting. Hmm...

		N = length(sizes)

		sizes_UInt8 = reinterpret(UInt8,sizes)
		io_checksums_UInt8 = reinterpret(UInt8,io_checksums)
		dates_UInt8 = reinterpret(UInt8,Int16.(
			getfield.(reshape(dates,2*N) .- Date("2021-01-01"),:value)
		))

		header = [sizes_UInt8;dates_UInt8;io_checksums_UInt8]
		header_checksum = reinterpret(UInt8,[crc32c(header)])

		return [UInt8(N);header_checksum;header]

	end

	# We don't want to copy the data...
	# 	ios = view(getfield.(array,:data),:1)
	# the below results in ios[1] === array[1].data = true sooo...
	# However with view you can actually change array, A by changing B if
	# B was created from A with view.
	# 	So we do is call the thing we actually want at the point of use?
	# 	Maybe?
	ios = getfield.(array,:data)
	# # ...but we want look up of small stuff to be easier.
	# Except we aren't storing that in a struct now.
	io_checksums() = getfield.(array,:data_crc32c)
	# Remember that Julia is column-major!
	sizes = sizeof.(ios)

	dates() = [getfield.(array,:src_date)	getfield.(array,:dst_date)]

	header() = mkheader(sizes,io_checksums(),dates())
	compressed_snappeds() = compress_data(ios,sum(sizes))

	return [header();compressed_snappeds()]

end

function write_snappeds(array::Array{UInt8})

	function encryptor()

		if test
			return `
				gpg --passphrase 1 --batch -c --s2k-mode 3 --s2k-cipher-algo AES256
			--s2k-digest-algo SHA512 --s2k-count 99999999 -o-`
		end

		println("\n\t\033[1m\e[0;33mPlease enter a password (twice)\033[0m\e[0m")
		password = readline()
		password2 = readline()

		if password != password2
			while password != password2
				println("The passwords given don't match")
				password = readline()
				password2 = readline()
			end
		end

		return `gpg --passphrase $password --batch -c --s2k-mode 3 --s2k-cipher-algo AES256 --s2k-digest-algo SHA512 --s2k-count 99999999 -o-`

	end

	# For more info on how to do this later:
	# https://www.linuxjournal.com/node/1005818
	function mk_self_extractor()

		# Maybe use a do block instead?
		# commit = cd(@__DIR__()) do
		# 	chomp(read(`git rev-parse --verify HEAD`,String)); end
		commit() = chomp(read(`git rev-parse --verify HEAD`,String))
		clean = repoclean ? "" : " not"

		str =
			"""
			#!/bin/julia -O0
			println("\nThis archive was created from commit\n
			$(cd(commit,@__DIR__()))\n
			with Julia Version $VERSION.\n
			The repo was$clean clean at the time.\n
			Attempting to self-extract")
			"""
			# The \n isn't needed, but it looks nicer this way.
			# Actually, I think it is now.

		return unsafe_wrap(Vector{UInt8},str)

	end

	temp_file = mktemp()
	crypt = encryptor()
	write(temp_file[2],mk_self_extractor(),open(@__DIR__()*"/receive.jl"))
	close(temp_file[2])

	open(pipeline(crypt,stdout=temp_file[1],append=true),write=true) do encrypt
		write(encrypt,array)
	end

	file_crc32c = bytes2hex(reverse(reinterpret(UInt8,[open(crc32c,temp_file[1])])))
	archive_date = getfirst(r"(20\d\d-\d\d-\d\d)",readdir(snaps_dir)[end])
	new_name = "Stories."*archive_date*".snap.$file_crc32c.zst.gpg"
	if test
		archive_dir_local = tempdir()*"/"
	else
		archive_dir_local = archive_dir*"/"
	end
	archive=mv(temp_file[1],archive_dir_local*new_name)
	chmod(archive,0o755)

	println("Archive located at: ",archive)

	return (archive=archive,password=crypt.exec[3])

end

# It may be best to start a Pipe(), then start the encryptor "listening" to the
# pipe, and then write to the pipe from `zstd`. Err... not that. How about:
# https://discourse.julialang.org/t/piping-into-an-external-program-from-julia-output/42691/7
snapped_array = cd(make_snappeds∘list_snaps,snaps_dir)
test_me = snappeds2bytes(snapped_array) |> write_snappeds

password = test_me.password
include(test_me.archive)
